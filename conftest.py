import pytest
from selenium import webdriver
from selenium.webdriver import Remote


def pytest_addoption(parser):
    """ Parse pytest --option variables from shell """
    parser.addoption('--browser', help='Which test browser?',
                     default='chrome')
    parser.addoption('--local', help='local or CI?',
                     choices=['true', 'false'],
                     default='true')


@pytest.fixture(scope='session')
def test_browser(request):
    """ :returns Browser.NAME from --browser option """
    return request.config.getoption('--browser')


@pytest.fixture(scope='session')
def local(request):
    """ :returns true or false from --local option """
    return request.config.getoption('--local')


@pytest.fixture(scope='function')
def remote_browser(test_browser, local) -> Remote:
    """ Select configuration depends on browser and host """
    local = local.lower()
    cmd_executor = {
        'true': 'http://localhost:4444/wd/hub',
        'false': f'http://selenium__standalone-{test_browser}:4444/wd/hub'
    }
    if test_browser == 'chrome':
        driver = webdriver.Remote(
            options=webdriver.ChromeOptions(),
            command_executor=cmd_executor[local],
        )
    else:
        raise ValueError(
            f'--browser="{test_browser}" is not chrome or firefox')
    driver.implicitly_wait(10)

    yield driver
    driver.quit()
